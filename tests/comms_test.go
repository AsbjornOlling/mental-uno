package tests

import (
  // std lib
  "testing"
  "time"

  // deps
  "github.com/FelixL321/gospace"

  // app imports
  . "../types"
  "../comms"
)

func Test_BroadcastMsg(t *testing.T) {
  // set up test vars
  p1 := Player{
    Id: "1",
    MsgSpaceAddr: "tcp://127.0.0.1:1331",
  }
  sp1 := gospace.NewSpace(p1.MsgSpaceAddr)

  p2 := Player{
    Id: "2",
    MsgSpaceAddr: "tcp://127.0.0.1:1332",
  }
  sp2 := gospace.NewSpace(p2.MsgSpaceAddr)

  me := Player{
    Id: "3",
  }

  players := []Player{
    p1,
    p2,
    me,
  }

  // wait for the spaces to come up...
  time.Sleep(150 * time.Millisecond)

  // do the test
  comms.BroadcastMsg(players, me, AckMsg, "foobar")

  var msgorigin string
  var msgtype MsgType
  var msgdata string
  var err error
  msgorigin, msgtype, msgdata, err = comms.ReadMsg(sp1)
  if err != nil || msgorigin != me.Id || msgtype != AckMsg || msgdata != "foobar" {
    t.Errorf("P1 did not receive broadcast tuple.")
  }

  msgorigin, msgtype, msgdata, err = comms.ReadMsg(sp2)
  if err != nil || msgorigin != me.Id || msgtype != AckMsg || msgdata != "foobar" {
    t.Errorf("P2 did not receive broadcast tuple.")
  }
}
