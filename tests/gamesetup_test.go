package tests

import (
  // std lib
  "reflect"
  "testing"

  // app imports
  . "../types"
  "../player"
  "../crapto"
)

func Test_GameSetup(t *testing.T) {
  gss, players, _ := SetupThreePlayers([]int{2234, 1235, 1236})
  gs1 := gss[0]
  gs2 := gss[1]
  gs3 := gss[2]

  // check that all players agree on playorder
  if !reflect.DeepEqual(gs1.Players, gs2.Players) ||
    !reflect.DeepEqual(gs2.Players, gs3.Players) ||
    !reflect.DeepEqual(gs2.Players, gs1.Players) {
    t.Error("Players do not agree on play order.")
  }

  // check that all players agree on shuffle
  if !reflect.DeepEqual(gs1.Deck, gs2.Deck) ||
    !reflect.DeepEqual(gs2.Deck, gs3.Deck) ||
    !reflect.DeepEqual(gs3.Deck, gs1.Deck) {
    t.Error("Players to not agree on deck.")
  }

  // check that all players agree on cmap
  if !reflect.DeepEqual(gs1.CardMap, gs2.CardMap) ||
    !reflect.DeepEqual(gs2.CardMap, gs3.CardMap) ||
    !reflect.DeepEqual(gs3.CardMap, gs1.CardMap) {
    t.Error("Players to not agree on CardMap.")
  }

  // check that all of g3's deck can be decrypted
  var keys []Pad
  var err error
  for i, encryptedcard := range gs3.Deck {
    keys = []Pad{gs1.DeckKeys[i],
      gs2.DeckKeys[i],
      gs3.DeckKeys[i]}
    _, err = crapto.DecryptCard(gs3.CardMap, encryptedcard, keys)
    if err != nil {
      t.Error(err)
    }
  }

  // check that all players have HANDSIZE cards
  for i := 0; i < 3; i++ {
    if len(gss[i].OwnHand) != HANDSIZE {
      t.Errorf("Player %d only has %d cards in OwnHand", i, len(gss[i].OwnHand))
    }
    if len(players[i].Hand) != HANDSIZE {
      t.Errorf("Player %d only has %d cards in Player.Hand", i, len(players[i].Hand))
    }
  }

  // check that all players agree contents of players' hands
  for _, p := range players {
    var gs1p Player
    var gs2p Player
    var gs3p Player
    gs1p, err = player.PlayerById(gs1.Players, p.Id)
    if err != nil {
      t.Error(err)
    }
    gs2p, err = player.PlayerById(gs2.Players, p.Id)
    if err != nil {
      t.Error(err)
    }
    gs3p, err = player.PlayerById(gs3.Players, p.Id)
    if err != nil {
      t.Error(err)
    }
    if !reflect.DeepEqual(gs1p.Hand, gs2p.Hand) ||
       !reflect.DeepEqual(gs2p.Hand, gs3p.Hand) ||
       !reflect.DeepEqual(gs3p.Hand, gs1p.Hand) {
      t.Error("Players don't agree on hands.")
    }
  }

  // check that all players agree on initial card
  if !reflect.DeepEqual(gs1.Lastcard, gs2.Lastcard) ||
    !reflect.DeepEqual(gs2.Lastcard, gs3.Lastcard) ||
    !reflect.DeepEqual(gs3.Lastcard, gs1.Lastcard) {
    t.Error("Players to not agree on initial card.")
  }
}
