package tests

import (
  "testing"

  "../art"
  "../frontend"
  . "../types"
)

func Test_PrintHand(t *testing.T) {
  var hand []DCard
  hand = append(hand, DCard{Type: Four, Color: Blue}, DCard{Type: Eight, Color: Red})
  hand = append(hand, DCard{Type: Reverse, Color: Yellow}, DCard{Type: Three, Color: Blue})
  hand = append(hand, DCard{Type: Zero, Color: Wild}, DCard{Type: PlusTwo, Color: Red})
  hand = append(hand, DCard{Type: Reverse, Color: Green}, DCard{Type: Two, Color: Green})
  hand = append(hand, DCard{Type: Reverse, Color: Wild}, DCard{Type: Skip, Color: Red})
  hand = append(hand, DCard{Type: Four, Color: Wild}, DCard{Type: Eight, Color: Red})
  hand = append(hand, DCard{Type: Reverse, Color: Wild}, DCard{Type: Three, Color: Blue})

  art.PrintHand(hand)
  frontend.PrintHand(hand)

}
