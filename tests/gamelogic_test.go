package tests

import (
  // std lib
  "testing"
  "fmt"
  "reflect"

  // app imports
  "../player"
  "../gamelogic"
  . "../types"
)

func Test_Draw(t *testing.T) {
  // run gamesetup, getting a working game for three players
  gss, players, spaces := SetupThreePlayers([]int{2233, 2244, 2255})

  // player 0's action
  var action Action = Action{AType: DrawAction,
                             DParams: DrawParams{DeckPos: 103}}

  // player 0 draws a random card
  outgoing := func(ch chan GameState) {
    gs, err := gamelogic.OutgoingDraw(spaces[0], gss[0], // player0 stuff
      players[0],        // player0 me
      0, action.DParams) // seqno, DrawParams
    if err != nil {
      t.Error(err)
    }
    ch <- gs
    return
  }

  // player i lets player 0 draw
  incoming := func(i int, ch chan GameState) {
    gs, err := gamelogic.IncomingDraw(spaces[i], gss[i], // playeri stuff
      players[i],     // playeri me
      players[0],     // action player
      action.DParams) // action params
    if err != nil {
      t.Error(err)
    }
    ch <- gs
    return
  }

  // copy pre-draw state of gss
  oldgss := make([]GameState, len(gss))
  copy(oldgss, gss)

  // run draw actions in parallel
  ch0 := make(chan GameState)
  ch1 := make(chan GameState)
  ch2 := make(chan GameState)
  go outgoing(ch0)
  go incoming(1, ch1)
  go incoming(2, ch2)
  gss[0] = <-ch0
  gss[1] = <-ch1
  gss[2] = <-ch2

  // check gamestates
  for i, newgs := range gss {
    // GameState.DrawPile should be one shorter on all gamestates
    if len(newgs.DrawPile) != len(oldgss[i].DrawPile)-1 {
      errmsg := fmt.Sprintf("GS%d.DrawPile not one shorter than previous state.", i)
      t.Error(errmsg)
    }

    // gs.Deck should be same length on all
    if len(newgs.Deck) != len(oldgss[i].Deck) {
      errmsg := fmt.Sprintf("GS%d.Deck not same length as previous state.", i)
      t.Error(errmsg)
    }
  }

  // first player ownhand check
  if len(gss[0].OwnHand) != len(oldgss[0].OwnHand)+1 {
    t.Error("Drawing player's OwnHand did not grow.")
  }

  // other players' OwnHand should be same length
  if len(gss[1].OwnHand) != len(oldgss[1].OwnHand) ||
    len(gss[2].OwnHand) != len(oldgss[2].OwnHand) {
    t.Error("Other player's ownhand did not stay same size.")
  }
}


// test draw action
func Test_Pass(t *testing.T) {
  var err error

  // run gamesetup, getting a working game for three players
  gss, players, _ := SetupThreePlayers([]int{3443, 3445, 3446})

  // pretend player0 has just played a card
  for i := 0; i < len(gss); i++ {
    gss[i].LastPlayedBy = players[0].Id
  }

  // player0 acts
  gss[0], err = gamelogic.OutgoingPass(gss[0], players[0], 0)
  if err != nil {
    t.Error(err)
  }

  // player1 reacts
  gss[1], err = gamelogic.IncomingPass(gss[1], players[0])
  if err != nil {
    t.Error(err)
  }

  // player2 reacts
  gss[2], err = gamelogic.IncomingPass(gss[2], players[0])
  if err != nil {
    t.Error(err)
  }

  if !reflect.DeepEqual(gss[0].CurrentPlayer, gss[1].CurrentPlayer) ||
     !reflect.DeepEqual(gss[1].CurrentPlayer, gss[2].CurrentPlayer) ||
     !reflect.DeepEqual(gss[2].CurrentPlayer, gss[0].CurrentPlayer) {
    t.Error("Players don't agree on current player.")
  }
}


func Test_Uno(t *testing.T) {
  var err error

  // run gamesetup, getting a working game for three players
  gss, players, _ := SetupThreePlayers([]int{1443, 1445, 3146})

  // all players only have one card
  for gsidx, gs := range gss {
    for pidx, p := range gs.Players {
      p.Hand = make([]ECard, 1)
      gs.Players[pidx] = p
    }
    gss[gsidx] = gs
  }
  // all players only have one card
  for i, p := range players {
    p.Hand = make([]ECard, 1)
    players[i] = p
  }

  // player0 plays uno
  gss[0], err = gamelogic.OutgoingUno(gss[0], players[0], 0)
  if err != nil {
    t.Error(err)
  }

  // player1 reacts
  gss[1], err = gamelogic.IncomingUno(gss[1], players[0])
  if err != nil {
    t.Error(err)
  }

  // player2 reacts
  gss[2], err = gamelogic.IncomingUno(gss[2], players[0])
  if err != nil {
    t.Error(err)
  }
}


//*  test play functionality
func Test_Play(t *testing.T) {
  // devnotes 
  // SEED 1-4: don't play
  // SEED 5:   gets runtime error, another error
  // SEED 18:  one player works, the other borks
  var err error

  // run gamesetup, getting a working game for three players
  gss, players, spaces := SetupThreePlayers([]int{8443, 8445, 8446})

  // quick check hand
  if len(gss[0].OwnHand) != HANDSIZE ||
     len(players[0].Hand) != HANDSIZE {
    t.Error("Hand error.")
  }

  // XXX: debug
  // check hand equality
  for _, p := range players {
    var gs1p Player
    var gs2p Player
    var gs3p Player
    gs1p, err = player.PlayerById(gss[0].Players, p.Id)
    if err != nil {
      t.Error(err)
    }
    gs2p, err = player.PlayerById(gss[1].Players, p.Id)
    if err != nil {
      t.Error(err)
    }
    gs3p, err = player.PlayerById(gss[2].Players, p.Id)
    if err != nil {
      t.Error(err)
    }
    // check
    if !reflect.DeepEqual(gs1p.Hand, gs2p.Hand) ||
       !reflect.DeepEqual(gs2p.Hand, gs3p.Hand) ||
       !reflect.DeepEqual(gs3p.Hand, gs1p.Hand) {
      t.Error("Players don't agree on hands.")
    }
  }

  // player0's play params
  handpos := 0
  params := PlayParams{Card: gss[0].OwnHand[handpos],
                       DeckPos: players[0].Hand[handpos].DeckPos,
                       Color: gss[0].OwnHand[handpos].Color,
                       HandPos: handpos}

  // make card playable
  for i, gs := range gss {
    gs.Lastcard = gss[0].OwnHand[handpos]
    gss[i] = gs
  }

  // func for p0 to play a card
  outgoing := func(ch chan GameState) {
    gs, err := gamelogic.OutgoingPlay(gss[0], players[0], 0, params)
    if err != nil {
      t.Error(err)
    }
    ch <- gs
  }

  // func for p1 & p2 to validate p0's play
  incoming := func(i int, ch chan GameState) {
    myplayers := gss[i].Players

    var mep Player
    var playerp Player
    // make sure players objects are the ones strictly from
    // a given players' gamestate
    mep, err = player.PlayerById(myplayers, players[i].Id)
    playerp, err = player.PlayerById(myplayers, players[0].Id)

    gs, err := gamelogic.IncomingPlay(spaces[i], gss[i], mep, playerp, params)
    if err != nil {
      t.Error(err)
    }
    ch <- gs
  }

  // run play action in parallel
  ch0 := make(chan GameState)
  ch1 := make(chan GameState)
  ch2 := make(chan GameState)
  go outgoing(ch0)
  go incoming(1, ch1)
  go incoming(2, ch2)
  gss[0] = <-ch0
  gss[1] = <-ch1
  gss[2] = <-ch2

  // check that player0 has one less card
  if len(gss[0].OwnHand) != HANDSIZE - 1 {
    t.Errorf("Players ownhand did not shrink after play. Size: %d", len(gss[0].OwnHand))
  }
  for i, gs := range gss {
    p, _ := player.PlayerById(gs.Players, players[0].Id)
    if len(p.Hand) != HANDSIZE - 1 {
      t.Errorf("P0's hand did not shrink for player %d. Size: %d", i, len(p.Hand))
    }
  }
} //*/


func Test_Specificmenu(t *testing.T) {
  //setting up gamestate
  var gs GameState
  gs.CurrentPlayer = "me"
  gs.Players = append(gs.Players, Player{Id: "me"})
  gs.CardDrawn = false
  gs.DrawPile = append(gs.DrawPile, ECard{})
  gs.PlayQueue = make([]string, 3)
  gs.PlayQueue = append(gs.PlayQueue, "me", "notme", "defnotme")
  gs.MenuOptions = make([]bool, 9)

  //Putting a blue reverse and red plustwo in hand. And green 2 as last played card
  gs.OwnHand = append(gs.OwnHand, DCard{Type: Reverse, Color: Blue}, DCard{Type: PlusTwo, Color: Red})
  gs.Lastcard = DCard{Color: Green, Type: Two}
  gamelogic.UpdateMenuOptions(gs, gs.Players[0])

  //No cards should be able to be played
  if gs.MenuOptions[1] {
    t.Error("Play card is shown, when no cards can be played")
  }
  if !gs.MenuOptions[2] {
    t.Error("Draw card is not shown, when it should")
  }

  //Menu option 7 and 8 should always be possible
  if !gs.MenuOptions[7] || !gs.MenuOptions[8] {
    t.Error("option 7 and 8 should always be possible")
  }

  // changing last card to a blue one
  gs.Lastcard = DCard{Type: 4, Color: Blue}

  gamelogic.UpdateMenuOptions(gs, gs.Players[0])
  //Play card should now be an option
  if !gs.MenuOptions[1] {
    t.Error("Play card is not shown, when blue card in hand and blue card was last played")
  }
  if gs.MenuOptions[2] {
    t.Error("Should not be able to draw when can play")
  }

  gs.Lastcard = DCard{Type: Reverse, Color: Green}
  gs.LastPlayedBy = "me"
  gamelogic.UpdateMenuOptions(gs, gs.Players[0])
  //Should be able to play but also to pass
  if !gs.MenuOptions[1] || !gs.MenuOptions[3] || gs.MenuOptions[2] {
    t.Error("Should be able to play and pass but not draw")
  }

  //Now is plus stacking
  gs.LastPlayedBy = "defnotme"
  gs.Lastcard = DCard{Type: PlusTwo, Color: Green}
  gs.PlusStack = 2

  gamelogic.UpdateMenuOptions(gs, gs.Players[0])
  //Should be able to play and draw but not pass
  if !gs.MenuOptions[1] || gs.MenuOptions[3] || gs.MenuOptions[2] {
    t.Error("Should be able to play but not pass and draw ")
  }

  gs.Players = append(gs.Players, Player{Id: "notme", Uno: false})
  gs.Players[1].Hand = append(gs.Players[1].Hand, ECard{})

  gamelogic.UpdateMenuOptions(gs, gs.Players[0])
  //Should be able to challenge uno now
  if !gs.MenuOptions[6] {
    t.Error("Should be able to challenge uno")
  }

  gs.OwnHand = gs.OwnHand[:1]
  gamelogic.UpdateMenuOptions(gs, gs.Players[0])

  //Should be able to call uno myself now
  if !gs.MenuOptions[4] {
    t.Error("Cant call uno even though i have one card in hand only")
  }

  gs.Lastcard.Type = PlusFour
  gamelogic.UpdateMenuOptions(gs, gs.Players[0])

  //Should be able to challenge the plus four now
  if !gs.MenuOptions[5] {
    t.Error("Cannot challenge plus even though last card is plus four")
  }

  gs.LastPlayedBy = "me"
  gamelogic.UpdateMenuOptions(gs, gs.Players[0])

  //Should no longer be able to challenge plus four as i played it myself
  if gs.MenuOptions[5] {
    t.Error("Should no longer be able to challenge plus four as i played it myself")
  }

}
