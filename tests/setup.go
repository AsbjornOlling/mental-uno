package tests

import (
  // std lib
  "time"
  "fmt"
  "log"

  // deps
  "github.com/FelixL321/gospace"

  // app imporas
  . "../types"
  "../gamesetup"
  "../player"
)


func SetupThreePlayers(ports []int) ([]GameState, []Player, []gospace.Space) {
  // set up test vars
  p1 := Player{
    Id: "1",
    Name: "DealerDave",
    MsgSpaceAddr: fmt.Sprintf("tcp://127.0.0.1:%d", ports[0]),
    Dealer: true,
  }
  sp1 := gospace.NewSpace(p1.MsgSpaceAddr)

  p2 := Player{
    Id: "2",
    Name: "SecondSam",
    MsgSpaceAddr: fmt.Sprintf("tcp://127.0.0.1:%d", ports[1]),
  }
  sp2 := gospace.NewSpace(p2.MsgSpaceAddr)

  p3 := Player{
    Id: "3",
    Name: "ThirdTherese",
    MsgSpaceAddr: fmt.Sprintf("tcp://127.0.0.1:%d", ports[2]),
  }
  sp3 := gospace.NewSpace(p3.MsgSpaceAddr)

  players := []Player{
    p1,
    p2,
    p3,
  }

  // function to run gamesetup concurrently
  runGameState := func(msgspace gospace.Space, players []Player, me Player, ch chan GameState) {
    playerscopy, err := player.CopyPlayers(players)
    if err != nil {
      log.Fatal("fuck your dog")
    }
    ch <- gamesetup.GameSetup(msgspace, playerscopy, me)
  }

  // make channels (to get gs's from goroutines)
  gs1chan := make(chan GameState)
  gs2chan := make(chan GameState)
  gs3chan := make(chan GameState)

  // run gamesetup concurrently
  go runGameState(sp1, players, p1, gs1chan)
  time.Sleep(500 * time.Millisecond) // SSSHHH
  go runGameState(sp2, players, p2, gs2chan)
  time.Sleep(500 * time.Millisecond) // NOBODY SAW THIS
  go runGameState(sp3, players, p3, gs3chan)

  // wait for gamesetup to finish
  gs1 := <-gs1chan
  gs2 := <-gs2chan
  gs3 := <-gs3chan

  // set p1 as starting player on all gamestates
  gs1.CurrentPlayer = p1.Id
  gs2.CurrentPlayer = p1.Id
  gs3.CurrentPlayer = p1.Id

  // get modified players from gamestate
  p1, _ = player.PlayerById(gs1.Players, p1.Id)
  p2, _ = player.PlayerById(gs2.Players, p2.Id)
  p3, _ = player.PlayerById(gs3.Players, p3.Id)

  return []GameState{gs1, gs2, gs3},
         []Player{p1, p2, p3},
         []gospace.Space{sp1, sp2, sp3}
}
