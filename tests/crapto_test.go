package tests

import (
  // std lib
  "testing"
  "reflect"

  // deps
  . "../types"
  "../crapto"
  "../gamelogic"
)

// test the XOR
func Test_OTP(t *testing.T) {
  key := crapto.NewRandomPad()
  plaintext := crapto.NewRandomPad()
  crypted := crapto.XOR(plaintext, key)
  decrypted := crapto.XOR(crypted, key)

  if decrypted != plaintext {
    t.Errorf("OTP did not decrypt correctly.\n")
  }
}

// test card decrypt / encrypt
func Test_EncryptDecrypt(t *testing.T) {
  ddeck := gamelogic.NewDeck()
  cmap := crapto.NewCardMap(ddeck)

  var keys []Pad = make([]Pad, 10)
  for i := 0; i < 10; i++ {
    keys[i] = crapto.NewRandomPad()
  }

  ecard := crapto.EncryptCard(cmap, ddeck[0], keys)
  dcard, err := crapto.DecryptCard(cmap, ecard, keys)
  if err != nil {
    t.Error(err)
  }

  if !reflect.DeepEqual(dcard, ddeck[0]) {
    t.Error("Card failed to decrypt.")
  }
}
