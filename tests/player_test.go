package tests

import (
  // std lib
  "testing"

  // app
  . "../types"
  "../player"
)


func Test_NextAndPrev(t *testing.T) {
  players := []Player{
    Player{Id: "0"},
    Player{Id: "1"},
    Player{Id: "2"},
  }

  var next Player
  var prev Player
  var err error

  next, prev, err = player.NextAndPrev(players, players[0])
  if err != nil {
    t.Error(err)
  }
  if next.Id != players[1].Id {
    t.Error("Wrong next player")
  }
  if prev.Id != players[2].Id {
    t.Error("Wrong prev player")
  }

  next, prev, err = player.NextAndPrev(players, players[1])
  if err != nil {
    t.Error(err)
  }
  if next.Id != players[2].Id {
    t.Error("Wrong next player")
  }
  if prev.Id != players[0].Id {
    t.Error("Wrong prev player")
  }

  next, prev, err = player.NextAndPrev(players, players[2])
  if err != nil {
    t.Error(err)
  }
  if next.Id != players[0].Id {
    t.Error("Wrong next player")
  }
  if prev.Id != players[1].Id {
    t.Error("Wrong prev player")
  }
}
