package tests

import (
  // std lib
  "testing"
  "time"

  // deps
  "github.com/FelixL321/gospace"
)

func Test_SelfConnect(t *testing.T) {
  space := gospace.NewSpace("tcp://127.0.0.1:1234/test")
  time.Sleep(200 * time.Millisecond)
  rspace := gospace.NewRemoteSpace("tcp://127.0.0.1:1234/test")

  space.Put(1337)

  var someint int
  tuple, _ := rspace.Get(&someint)

  if tuple.Fields[0] != 1337 {
    t.Error("Failed getting number for (locally emulated) remote space")
  }

  return
}


/*
// test concurrently listening on the same space
// first listen for an int, then on a string
// but put in string first
func Test_DoubleConnect(t *testing.T) {
  space := gospace.NewSpace("TESTSPACE")

  getInt := func(sp gospace.Space, gotint chan int) {
    var someint int
    tuple, _ := sp.Get(&someint)
    gotint <- tuple.Fields[0].(int)
    return
  }

  getStr := func(sp gospace.Space, gotstr chan string) {
    var somestr string
    tuple, _ := sp.Get(&somestr)
    gotstr <- tuple.Fields[0].(string)
    return
  }

  // start the int getter
  gotint := make(chan int)
  go getInt(space, gotint)
  log.Print("Started int getter")
  time.Sleep(100 * time.Millisecond)

  // start the string getter
  gotstr := make(chan string)
  go getStr(space, gotstr)
  log.Print("Started string getter")

  // put in a string
  space.Put("foobar")

  // put in an int
  // space.Put(1337)

  // wait for string getter to compete
  thestr := <-gotstr
  log.Print("Got string: ", thestr)

  // theint := <-gotint
  // log.Print("Got int: ", theint)

  if thestr != "foobar" {
    t.Error("Didn't get the str.")
  }
  // if theint != 1337 {
  //   t.Error("Didn't get the int.")
  // }
  return
}
*/
