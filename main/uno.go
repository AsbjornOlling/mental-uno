package main

/*  THIS FILE IS MADE FROM HACKY GLUE
 *  I ENCOURAGE YOU TO NOT THINK TOO HARD ABOUT IT.
 */

import (
  // std lib
  "strings"
  "fmt"
  "strconv"
  "flag"
  "math/rand"
  "time"

  // Deps
  . "github.com/FelixL321/gospace"

  // App imports
  "../gamelogic"
  "../gamesetup"
  "../lobby"
  "../types"
  "../frontend"
  "../player"
)


// Get host/port arguments or default to localhost:31415
func args() (addr string) {
  flag.Parse()
  argn := flag.NArg()
  if argn >= 2 {
    frontend.ARGFail()
    return
  } else if argn >= 1 {
    addr = flag.Arg(0)
  } else {
    addr = "localhost:6666"
  }
  return addr
}


func main() {
  // let the random begin!
  rand.Seed(time.Now().UnixNano())

  host := args()
  // (workaround space has to be set up before lobby)
  // (so peers can connect immediately when leaving lobby)
  var hackspace Space = NewSpace(newSpaceAddr(host))

  // do lobby - let players connect to each other
  var players []types.Player
  players, _ = lobby.Lobby(host)
  myId := players[0].Id

  // REPLACE THE SPACES
  // ITS A HACK
  // ITS A LAST MINUTE HACK
  // DONT ASK BOUT THE HASK
  hackplayers := make([]types.Player, len(players))
  for i, p := range players {
    p.MsgSpaceAddr = newSpaceAddr(p.MsgSpaceAddr)
    hackplayers[i] = p
  } // SSSSHSHHH

  gs := gamesetup.GameSetup(hackspace, hackplayers, hackplayers[0])

  hackme, _ := player.PlayerById(gs.Players, myId)
  gamelogic.GameLoop(hackspace, gs, hackme)
}


// NO AHX
// NO NO N O
func newSpaceAddr(oldSpaceAddr string) string {
  var splits []string = strings.Split(oldSpaceAddr, ":")
  if len(splits) == 3 {
    var portstr string  = splits[2]
    portint, _ := strconv.Atoi(portstr)
    var newaddr = fmt.Sprintf("%s:%s:%d", splits[0], splits[1], portint + 1)
    return newaddr
  } else if len(splits) == 2 {
    var portstr string = splits[1]
    portint, _ := strconv.Atoi(portstr)
    var newaddr = fmt.Sprintf("tcp://%s:%d", splits[0], portint + 1)
    return newaddr
  }
  return "urmom"
}
// haaaaaaaaaaaax

