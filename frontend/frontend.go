package frontend

/*
🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙
🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙
🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙
🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙
🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙
🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙
🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙
🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙
🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙
🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙
🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙
🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙🤙
*/
import (
  // stdlib
  "bufio"
  "fmt"
  "log"
  "math/rand"
  "os"
  "strconv"
  "strings"

  // app imports
  "../art"
  "../player"
  . "../types"
)

// converting enum to string
func colorConvert(c CardColor) (returnColor string) {
  switch c {
  case Red:
    returnColor = "Red "
  case Blue:
    returnColor = "Blue "
  case Yellow:
    returnColor = "Yellow "
  case Green:
    returnColor = "Green "
  case Wild:
    returnColor = "Wild "
  }
  return
}

// converting enum to string
func typeConvert(ct CardType) (returnType string) {
  switch ct {
  case Zero:
    returnType = "0"
  case One:
    returnType = "1"
  case Two:
    returnType = "2"
  case Three:
    returnType = "3"
  case Four:
    returnType = "4"
  case Five:
    returnType = "5"
  case Six:
    returnType = "6"
  case Seven:
    returnType = "7"
  case Eight:
    returnType = "8"
  case Nine:
    returnType = "9"
  case Skip:
    returnType = "Skip"
  case Reverse:
    returnType = "Reverse"
  case PlusTwo:
    returnType = "Draw two"
  case PlusFour:
    returnType = "Draw four"
  case Change:
    returnType = ""
  }
  return
}

func printCard(card DCard) string {
  return colorConvert(card.Color) + " " + typeConvert(card.Type)
}

// get menu option from a maximum number of options
func GetMenuOption(max int) (opt int) {
  var err error
  for opt < 1 || opt > max {
    fmt.Println("Pick an option")
    reader := bufio.NewReader(os.Stdin)
    var option string
    if option, err = reader.ReadString('\n'); err != nil {
      opt = -1
      continue
    }
    opt, err = strconv.Atoi(strings.TrimSuffix(option, "\n"))
    if err != nil {
      opt = -1
      continue
    }
  }
  return opt
}

func PrintPlayedCard(card DCard, name string) {
  art.PrintDCard(card)
  fmt.Printf("Player %s played a %s\n", name, printCard(card))
}

func PrintHand(hand []DCard) {
  for i := 0; i < len(hand); i++ {
    fmt.Printf("(%d) "+printCard(hand[i])+"\n", i+1)
  }
}

func PickCard(gs GameState) int {
  art.PrintHand(gs.OwnHand)
  PrintHand(gs.OwnHand)
  return getMenuOptionSpecific(len(gs.OwnHand), gs.CardOptions)
}

func PickColor() CardColor {
  fmt.Println(`Please pick the next color
  (1) Red
  (2) Blue
  (3) Yellow
  (4) Green`)
  return CardColor(GetMenuOption(4))
}

//printing only valid menu options
func printActionMenu(gs GameState) (option []bool) {
  fmt.Println("Pick an action:")
  if gs.MenuOptions[1] {
    fmt.Println("(1) Play card")
  }
  if gs.MenuOptions[2] {
    fmt.Println("(2) Draw card")
  }
  if gs.MenuOptions[3] {
    fmt.Println("(3) Pass turn")
  }
  if gs.MenuOptions[4] {
    fmt.Println("(4) Call uno")
  }
  if gs.MenuOptions[5] {
    fmt.Println("(5) Challenge plus four")
  }
  if gs.MenuOptions[6] {
    fmt.Println("(6) Challenge Uno")
  }
  if gs.MenuOptions[7] {
    fmt.Println("(7) View nr of cards of each player")
  }
  if gs.MenuOptions[8] {
    fmt.Println("(8) Show own hand")
  }

  return
}

//Get specific menu options depending on a slice of bools
func getMenuOptionSpecific(max int, slice []bool) (opt int) {
  for {
    opt = GetMenuOption(max)
    if slice[opt] {
      break
    }
  }
  return opt
}

// multi-step menu in which user decides on an action
// not done at all
func ActionMenu(gs GameState) Action {
  for {
    //printing only viable options
    printActionMenu(gs)
    //only getting viable options
    choice := getMenuOptionSpecific(8, gs.MenuOptions)

    // construct action based on choice
    switch choice {
    case 1:
      //Picking a card form hand
      handpos := PickCard(gs) - 1

      //picking next color in case playing a wild card
      var color CardColor
      if gs.OwnHand[handpos].Color == Wild {
        color = PickColor() - 1
      }
      //Returning Action struct
      curplayer, _ := player.PlayerById(gs.Players, gs.CurrentPlayer)
      deckpos := curplayer.Hand[handpos].DeckPos
      return Action{PParams: PlayParams{Card: gs.OwnHand[handpos], Color: CardColor(color), HandPos: handpos, DeckPos: deckpos}, AType: PlayAction}
    case 2:
      // decide on random deckid to draw
      // pick card to draw
      deckpos := gs.DrawPile[rand.Intn(len(gs.DrawPile))].DeckPos
      return Action{AType: DrawAction,
        DParams: DrawParams{DeckPos: deckpos}}

    case 3:
      return Action{AType: PassAction}

    case 4:
      return Action{AType: UnoAction}

    case 5:
      // TODO: implement
      log.Fatal("Not implemented yet.")
      // return Action{ChallengePlusFourAction}

    case 6:
      // TODO: impelement
      log.Fatal("Not implemented yet.")
      // return ChallengeUnoAction

    case 7:
      PrintHandSizes(gs)
    }
  }
  // failure
  return Action{}
}

func PrintHandSizes(gs GameState) {
  for i := 0; i < len(gs.Players); i++ {
    fmt.Printf("%s has %d cards in hand\n", gs.Players[i].Name, len(gs.Players[i].Hand))
  }
}

func InitialCard(card DCard) {
  fmt.Println("The initial card is")
  art.PrintDCard(card)
}

func PlayerFinished(player Player) {
  fmt.Println(player.Name, " has finished the game")
}

func GameHasFinished() {
  fmt.Println("GAME OVER!!!!")
}

/*
 *******************************************************************************
 * m             #      #                             m             m""    m"" *
 * #       mmm   #mmm   #mmm   m   m          mmm   mm#mm  m   m  mm#mm  mm#mm *
 * #      #" "#  #" "#  #" "#  "m m"         #   "    #    #   #    #      #   *
 * #      #   #  #   #  #   #   #m#           """m    #    #   #    #      #   *
 * #mmmmm "#m#"  ##m#"  ##m#"   "#           "mmm"    "mm  "mm"#    #      #   *
 *                              m"                                             *
 *                             ""                                              *
 *******************************************************************************
 */

// Lobby inputs
func ReadLine() (txtline string) {
  reader := bufio.NewReader(os.Stdin)
  txtline, err := reader.ReadString('\n')
  if err != nil {
    log.Println("Could not read string, due to: %s", err)
    return
  }
  txtline = strings.TrimSuffix(txtline, "\n")
  return txtline
}

// Lobby prints
func ARGFail() {
  fmt.Println("Usage of Mental UNO: [adress]:[port]")
}

func PrintWelcome() string {
  fmt.Println("Welcome to Mental UNO lobby")
  fmt.Print("Enter nick: ")
  return ReadLine()
}

func PrintGametypeMenu(name string) int {
  fmt.Printf("Hey %s please choose what you want to do: \n", name)
  fmt.Println(`(1) Start a game
(2) Join a game`)
  return GetMenuOption(2)
}

func StartGameLobby(host string) {
  fmt.Println("You have started a game lobby")
  fmt.Printf("People can join this lobby at: %s\n", host)
  fmt.Println("Waiting for first player")
}

func PrintJoinedLobby(name string) {
  fmt.Printf("%s joined the lobby\n", name)
}

func RemHostMenu() string {
  fmt.Println("Enter a IP:port of the game lobby you want to join ")
  return ReadLine()
}

func PrintConnTo(remhost string) {
  fmt.Printf("Connecting to: %s\n", remhost)
}

func PrintJoinedLobbyOf(name string, host string) {
  fmt.Printf("Joined the game of %s\n", name)
  fmt.Printf("People can now join at: %s\n", host)
}

func WaitReadyMenu() int {
  fmt.Println("Waiting for more players or until all players is ready")
  fmt.Println(`While waiting you can press:
(1) To broadcast ready
(2) To list players in this lobby
(3) To broadcast a message to the other players`)
  return GetMenuOption(3)
}

func PrintReadyPlayer(player Player) {
  fmt.Printf("Playernick: %s  Status: Ready\n", player.Name)
}

func PrintNotReadyPlayer(player Player) {
  fmt.Printf("Playernick: %s  Status: Not Ready\n", player.Name)
}

func PrintSendChat() string {
  fmt.Println("Enter your chat message:")
  return ReadLine()
}

func PrintRecvChat(nick string, msgdata string) {
  fmt.Printf("%s: %s\n", nick, msgdata)
}
