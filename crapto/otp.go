package crapto

import (
  // std lib
  "math/rand"

  // app imports
  . "../types"
)

func NewRandomPad() Pad {
  key := Pad{}
  for i := 0; i < PADSIZE; i++ {
    key[i] = byte(rand.Intn(256))
  }
  return key
}

// encrypt or decrypt using XOR
func XOR(plaintext, key Pad) Pad {
  result := Pad{}
  for i := 0; i < PADSIZE; i++ {
    result[i] = plaintext[i] ^ key[i]
  }
  return result
}
