package crapto

import (
  // std lib
  "errors"

  // app imports
  . "../types"
)

func CryptCard(ecard ECard, key Pad) ECard {
  return ECard{
    DeckPos: ecard.DeckPos,
    Data:    XOR(ecard.Data, key),
  }
}


// decrypt card using N keys
func DecryptCard(cmap CardMap, ecard ECard, keys []Pad) (DCard, error) {
  // decrypt using all keys
  var decrypted ECard = ecard
  for _, key := range keys {
    decrypted = CryptCard(decrypted, key)
  }

  // look up decrypted card in cardsmap,
  dcard, found := cmap.DCards[decrypted.Data]
  if !found {
    return DCard{}, errors.New("Could not find decrypted card in cardsmap.")
  }
  return dcard, nil
}


// encrypt a dcard with N keys, returning the ecard
func EncryptCard(cmap CardMap, dcard DCard, keys []Pad) ECard {
  // get pad mapping to dcard
  cardpad := cmap.CardPads[dcard]

  // encrypt N times
  var encrypted ECard = ECard{Data: cardpad}
  for _, key := range keys {
    encrypted = CryptCard(encrypted, key)
  }
  return encrypted
}
