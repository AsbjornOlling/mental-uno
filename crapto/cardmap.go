package crapto

// app imports
import . "../types"

// maps dcards in a given deck to random values of size PADSIZE
// and the other way
func NewCardMap(deck []DCard) CardMap {
  dcards := make(map[Pad]DCard)
  cardpads := make(map[DCard]Pad)
  for _, dcard := range deck {
    pad := NewRandomPad()
    dcards[pad] = dcard
    cardpads[dcard] = pad
  }
  return CardMap{DCards: dcards,
                 CardPads: cardpads}
}
