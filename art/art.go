package art

import (
  // std lib
  "fmt"
  "io/ioutil"
  "log"
  "os"
  "strings"

  // app imports
  . "../types"
)

const DEFAULT string = "\033[0m"
const BLACK string = "\033[30m"
const RED string = "\033[31m"
const GREEN string = "\033[32m"
const YELLOW string = "\033[33m"
const BLUE string = "\033[34m"

func PrintHand(hand []DCard) {
  if len(hand) == 0 {
    return
  }

  arts := make([][]string, len(hand))
  for i, card := range hand {
    arts[i] = cardArt(card.Type)
  }

  maxlines := len(arts[0])
  for lineno := 0; lineno < maxlines; lineno++ {
    for i, card := range hand {
      fmt.Print(cardColor(card.Color), arts[i][lineno], DEFAULT)
    }
    fmt.Print("\n")
  }
}

func PrintDCard(dcard DCard) {
  art := cardArt(dcard.Type)
  color := cardColor(dcard.Color)
  for _, artline := range art {
    fmt.Println(color, artline)
  }
  fmt.Print(DEFAULT)
}

// get color for a given card color
func cardColor(color CardColor) string {
  switch color {
  case Red:
    return RED
  case Blue:
    return BLUE
  case Yellow:
    return YELLOW
  case Green:
    return GREEN
  case Wild:
    return BLACK
  }
  return DEFAULT
}

// get lines of ascii art for a given card type
func cardArt(ctype CardType) []string {
  var path string
  switch ctype {
  case Zero:
    path = "0.art"
  case One:
    path = "1.art"
  case Two:
    path = "2.art"
  case Three:
    path = "3.art"
  case Four:
    path = "4.art"
  case Five:
    path = "5.art"
  case Six:
    path = "6.art"
  case Seven:
    path = "7.art"
  case Eight:
    path = "8.art"
  case Nine:
    path = "9.art"
  case Skip:
    path = "skip.art"
  case Reverse:
    path = "reverse.art"
  case PlusTwo:
    path = "plus2.art"
  case PlusFour:
    path = "plus4.art"
  case Change:
    path = "change.art"
  }
  path = fmt.Sprintf("../art/%s", path)
  return readFile(path)
}

// return contents of text file at `path` as string
func readFile(path string) []string {
  // open file
  file, err := os.Open(path)
  if err != nil {
    errmsg := fmt.Sprintf("Could not open file: %s", path)
    log.Fatal(errmsg)
  }
  defer file.Close()

  // read file
  bytes, err := ioutil.ReadAll(file)

  lines := strings.Split(string(bytes), "\n")
  return lines
}
