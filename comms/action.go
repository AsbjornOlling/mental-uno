package comms

import (
  // deps
 "github.com/FelixL321/gospace"

 // app imports
 . "../types"
)


// send a game action tuple (seqno, userid, actiontype, actiondata) to all players (except me)
func BroadcastAction(players []Player, me Player,
                     seqno int, actiontype ActionType, actiondata string) error {
  for _, dst := range players {
    if dst.Id != me.Id { // don't send to self
      err := SendAction(dst, me, seqno, actiontype, actiondata)
      if err != nil {
        return err
      }
    }
  }
  return nil
}


// send a game action tuple (seqno, userid, actiontype, actiondata) to one player
func SendAction(dst Player, me Player,
                seqno int, actiontype ActionType, actiondata string) error {
  // connect to space
  space := gospace.NewRemoteSpace(dst.MsgSpaceAddr)
  // put action in space
  _, err := space.Put(seqno, me.Id, string(actiontype), actiondata)
  return err
}


// read a game action with a **specific sequence number**
// returns (userid, actiontype, actiondata)
func ReadAction(msgspace gospace.Space, seqno int) (string, ActionType, string, error) {
  var userid string
  var actiontype string
  var actiondata string
  tuple, err := msgspace.Get(seqno, &userid, &actiontype, &actiondata)
  if err != nil {
    return "", ActionType(""), "", err
  }
  return tuple.Fields[1].(string),
         ActionType(tuple.Fields[2].(string)),
         tuple.Fields[3].(string),
         nil
}


func ExpectActionFrom(msgspace gospace.Space, seqno int,
                      actiontype ActionType, playerid string) (string, error) {
  var actiondata string
  tuple, err := msgspace.Get(seqno, playerid, string(actiontype), &actiondata)
  if err != nil {
    return "", err
  }

  _ = actiontype
  _ = seqno
  _ = playerid
  _ = actiondata

  return tuple.Fields[3].(string), nil
}
