package comms

/* comms.go
 * Contains functions to interact with other players' tuple spaces.
 * Basically either broadcast or wait for tuple on the spaces.
 */

import (
  // deps
  "github.com/FelixL321/gospace"

  // app imports
  . "../types"
)

// sends (sender, msgtype, msgdata) tuple to all other players' msg spaces
func BroadcastMsg(players []Player, me Player,
  msgtype MsgType, msgdata string) error {
  for _, p := range players {
    if p.Id != me.Id { // don't send to self
      err := SendMsg(p, me, msgtype, msgdata)
      if err != nil {
        return err
      }
    }
  }
  return nil
}

// send (sender, msgtype, msgdata) tuple to one player
func SendMsg(dst Player, me Player, msgtype MsgType, msgdata string) error {
  // TODO: making new space connections on each broadcast is hella dumb
  //       we should ideally only connect once, and then re-use the space
  space := gospace.NewRemoteSpace(dst.MsgSpaceAddr)
  // put msg in space
  _, err := space.Put(me.Id, string(msgtype), msgdata)
  return err
}

// read a message on the messge space
// returns (userid, msgtype, msgdata)
func ReadMsg(msgspace gospace.Space) (string, MsgType, string, error) {
  var userid string
  var msgtype string
  var msgdata string
  tuple, err := msgspace.Get(&userid, &msgtype, &msgdata)
  if err != nil {
    return "", MsgType(""), "", err
  }

  return tuple.Fields[0].(string),
    MsgType(tuple.Fields[1].(string)),
    tuple.Fields[2].(string),
    nil
}

// wait for message of specific type on message space
// returns (userid, msgdata)
func ExpectMsg(msgspace gospace.Space, msgtype MsgType) (string, string, error) {
  var userid string
  var msgdata string
  tuple, err := msgspace.Get(&userid, string(msgtype), &msgdata)
  if err != nil {
    return "", "", err
  }
  return tuple.Fields[0].(string),
    tuple.Fields[2].(string),
    nil
}

// expect message from specific player
// returns msgdata
func ExpectMsgFrom(msgspace gospace.Space,
  msgtype MsgType, playerid string) (string, error) {
  var msgdata string
  tuple, err := msgspace.Get(playerid, string(msgtype), &msgdata)
  if err != nil {
    return "", err
  }
  return tuple.Fields[2].(string), nil
}

// expect messge from all players, returns array of
func ExpectMsgFromAll(msgspace gospace.Space, players []Player,
  me Player, msgtype MsgType) ([]string, error) {
  var results []string
  for _, player := range players {
    // don't expect from self
    if player.Id == me.Id {
      continue
    }

    // wait for message from specific player
    msgdata, err := ExpectMsgFrom(msgspace, msgtype, player.Id)
    if err != nil {
      return nil, err
    }

    // put in results slice
    results = append(results, msgdata)
  }
  return results, nil
}
