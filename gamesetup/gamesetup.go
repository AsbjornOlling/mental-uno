package gamesetup

import (
  // std lib
  "fmt"
  "log"

  // deps
  "github.com/FelixL321/gospace"

  // app imports
  "../player"
  . "../types"
)

// game setup phase
// build a game state with other players,
// agreeing on play order and deck shuffle
func GameSetup(msgspace gospace.Space, players []Player, me Player) GameState {
  // let dealer decide on player order
  // results in a player slice that everyone agrees on
  playorder, err := playOrderState(msgspace, players, me)
  if err != nil {
    log.Fatal("SETUP: Setting player order failed: ", err)
  }
  log.Println("SETUP: Play order set.")

  // init deck and share deck mapping hashes
  var hashes [][32]byte
  var cmap CardMap
  var edeckplain []ECard // only dealer populates this one
  if me.Dealer {
    // dealer creates deck and mapping to pads
    cmap, edeckplain = createDeck()
    // share the hashes
    hashes, err = broadcastCardMapHashes(players, me, edeckplain)
    if err != nil {
      log.Fatal("SETUP: Failed sharing pad hashes.")
    }
  } else {
    // read the hashes from dealer
    hashes, err = recvCardMapHashes(msgspace)
    if err != nil {
      log.Fatal("SETUP: Failed receiving pad hashes.")
    }
  }
  log.Println("SETUP: Pad hash broadcast completed.")

  // shuffle cards together
  var shufdeck []ECard
  var deckkeys []Pad
  shufdeck, deckkeys, err = shuffleState(msgspace, playorder, me, edeckplain)
  if err != nil {
    log.Fatal("SETUP: Shuffling deck failed: ", err)
  }
  log.Println("SETUP: Shuffle completed.")

  // share cardmap with everyone
  if me.Dealer {
    err = broadcastCardMap(players, me, edeckplain)
    if err != nil {
      log.Fatal("SETUP: Failed broadcasting cardmap.")
    }
  } else {
    cmap, err = recvCardMap(msgspace, hashes)
    if err != nil {
      log.Fatal("SETUP: Failed receiving cardmap.")
    }
  }
  log.Println("SETUP: CardMap shared.")

  // make drawpile separate from reference deck
  // (needs to be copy, not same reference)
  var drawpile []ECard = make([]ECard, len(shufdeck))
  copy(drawpile, shufdeck)

  // assemble gamestate object and return
  gs := GameState{
    // card info
    Deck:     shufdeck,
    DrawPile: drawpile,
    DeckKeys: deckkeys,
    CardMap:  cmap,

    // player info
    Players:   playorder,
    PlayQueue: player.PlayerIds(playorder),

    // gamestate
    CurrentPlayer: playorder[0].Id,
    Done:          false,
    MenuOptions:   make([]bool, 9),
  }
  // all players draw cards
  gs, err = drawState(msgspace, gs, me)
  if err != nil {
    errmsg := fmt.Sprintf("SETUP: Failed dealing cards: %s", err)
    log.Fatal(errmsg)
  }
  // play first card
  gs, err = playInitCard(msgspace, players, gs, me)
  if err != nil {
    errmsg := fmt.Sprintf("SETUP: Failed playing first card: %s", err)
    log.Fatal(errmsg)
  }
  return gs
}
