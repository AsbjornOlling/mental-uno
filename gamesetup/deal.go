package gamesetup

/* deal.go
 * Functions associated with the initial dealing of hands.
 * Each player draws HANDSIZE cards, taking turns to do so.
 */

import (
  // std lib
  "encoding/json"
  "errors"
  "fmt"
  "log"
  "math/rand"
  "strconv"

  // deps
  "github.com/FelixL321/gospace"

  // app imports
  "../comms"
  "../crapto"
  "../frontend"
  "../gamelogic"
  "../player"
  . "../types"
)

func drawState(msgspace gospace.Space,
  gs GameState, me Player) (GameState, error) {
  var err error
  // take turns drawing cards, following normal play order
  for _, drawerid := range gs.PlayQueue {
    // find drawing player obj
    var drawer Player
    drawer, err = player.PlayerById(gs.Players, drawerid)
    if err != nil {
      return GameState{}, err
    }

    // ITS DANGEROUS TO GO ALONE
    // TAKE THIS
    gs.PlusStack = 7
    gs.CurrentPlayer = drawerid
    // ssshhh its a hack
    // but it works

    if me.Id == drawerid {
      // when it's my turn

      // draw HANDSIZE cards
      for i := 0; i < HANDSIZE; i++ {
        // decide on random deckid to draw
        // pick card to draw
        deckpos := gs.DrawPile[rand.Intn(len(gs.DrawPile))].DeckPos
        drawparams := DrawParams{DeckPos: deckpos}

        // do the collaborative draw
        gs, err = gamelogic.OutgoingDraw(msgspace, gs, me, i, drawparams)
        if err != nil {
          return GameState{}, err
        }
      }

      // when it's somebody else's turn
    } else {

      // help drawer in drawing HANDSIZE cards
      for i := 0; i < HANDSIZE; i++ {
        // wait for draw action from drawer
        var actiondata string
        actiondata, err = comms.ExpectActionFrom(msgspace, i,
          DrawAction, drawerid)
        if err != nil {
          return GameState{}, err
        }

        // parse draw action
        var params DrawParams
        err = json.Unmarshal([]byte(actiondata), &params)
        if err != nil {
          return GameState{}, err
        }

        // help drawer draw
        gs, err = gamelogic.IncomingDraw(msgspace, gs, me, drawer, params)
        if err != nil {
          return GameState{}, err
        }
      }
    }
    gs, err = gamelogic.Pass(gs, drawer, true)
    if err != nil {
      return GameState{}, err
    }
  } // </for>
  return gs, nil
}

func playInitCard(msgspace gospace.Space, players []Player, gs GameState, me Player) (GameState, error) {
  var deckpos int
  if me.Dealer {

    //Dealer chooses the card
    rand.Intn(len(gs.DrawPile))

    deckpos = gs.DrawPile[rand.Intn(len(gs.DrawPile))].DeckPos
    comms.BroadcastMsg(players, me, InitCardMsg, strconv.Itoa(deckpos))
  } else {

    //everybody else waits for the chosen card
    _, msgdata, err := comms.ExpectMsg(msgspace, InitCardMsg)
    if err != nil {
      log.Fatal("Comms went wrong in init card")
    }
    deckpos, err = strconv.Atoi(msgdata)
    if err != nil {
      log.Fatal("String convert fucked up")
    }
  }

  //Now everybody has to send their keys to everyone
  drawkey := gs.DeckKeys[deckpos]
  drawkeyjson, err := json.Marshal(drawkey)
  if err != nil {
    log.Fatal("marshal gone wrong way down the roaaaaaaad")
    return GameState{}, err
  }
  err = comms.BroadcastMsg(players, me, KeyRevealMsg, string(drawkeyjson))
  if err != nil {
    log.Fatal("Broadcast of keys gone wrong")
    return GameState{}, err
  }

  // expect keys from all
  var keysjson []string
  keysjson, err = comms.ExpectMsgFromAll(msgspace, gs.Players, me, KeyRevealMsg)
  if err != nil {
    errmsg := fmt.Sprintf("Error while getting keys for draw: %s", err)
    return GameState{}, errors.New(errmsg)
  }

  // deserialize keys
  var keys []Pad = make([]Pad, len(gs.Players))
  for i, keyjson := range keysjson {
    err = json.Unmarshal([]byte(keyjson), &keys[i])
    if err != nil {
      errmsg := fmt.Sprintf("Error while decoding keys: %s", err)
      return GameState{}, errors.New(errmsg)
    }
  }

  // add own key
  keys[len(keys)-1] = gs.DeckKeys[deckpos]

  // decrypt card
  var dcard DCard
  dcard, err = crapto.DecryptCard(gs.CardMap, gs.Deck[deckpos], keys)
  if err != nil {
    return GameState{}, err
  }

  //remove card drawpile
  var found bool = false
  for i, pilecard := range gs.DrawPile {
    if pilecard.DeckPos == deckpos {
      gs.DrawPile = append(gs.DrawPile[:i], gs.DrawPile[i+1:]...)
      found = true
    }
  }
  if !found {
    errmsg := fmt.Sprintf("Drawn card not found in DrawPile: %d", deckpos)
    return gs, errors.New(errmsg)
  }

  //update lastcard
  if dcard.Color == Wild {
    //DEfault to red
    gs.Lastcard.Color = Red
    log.Println("card Defaulted to red in init card")
    gs.Lastcard.Type = dcard.Type
  } else {
    gs.Lastcard = dcard
  }

  frontend.InitialCard(dcard)

  return gs, nil
}
