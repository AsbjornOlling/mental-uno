package gamesetup

/* shuffle.go
 * Functions associated with the shuffle phase of game setup.
 * Each player XOR's all cards with the same key, then shuffles
 * and passes the deck on to the next player.
 * When all players have shuffled the deck, no-one knows the position of any card.
 * At this time, all players remove their initial key, and replace the encryption on
 * each card with a key unique to that card.
 * Dealer has a special role, since this is the player defined to start and stop the
 * rounds of shuffling.
 */

import (
  // std lib
  "encoding/json"
  "errors"
  "fmt"
  "math/rand"

  // deps
  "github.com/FelixL321/gospace"

  // app imports
  "../comms"
  "../crapto"
  "../player"
  . "../types"
)

// shuffle cards with the other players
func shuffleState(msgspace gospace.Space, players []Player,
  me Player, edeckplain []ECard) ([]ECard, []Pad, error) {
  // find next and previous player
  // in turn order
  nextp, prevp, err := player.NextAndPrev(players, me)
  if err != nil {
    return nil, nil, err
  }

  if !me.Dealer {
    // wait for shuffled deck to arrive
    edeckplain, err = recvEDeck(msgspace, prevp, ShuffleMsg)
    if err != nil {
      errmsg := fmt.Sprintf("%s non-dealer waiting for deck from prev: %s", me.Name, err)
      return nil, nil, errors.New(errmsg)
    }
  }

  // encrypt and shuffle
  var shufkey Pad = crapto.NewRandomPad()
  xored := cryptAll(edeckplain, shufkey)
  shuffled := shuffleEDeck(xored)

  // send deck to next player
  err = sendEDeck(nextp, me, ShuffleMsg, shuffled)
  if err != nil {
    return nil, nil, err
  }

  var rekeydeck []ECard
  if me.Dealer {
    // dealer should wait for final shuffle to arrive,
    // and then start re-keying phase
    // var finalshuffle []ECard
    rekeydeck, err = recvEDeck(msgspace, prevp, ShuffleMsg)
    if err != nil {
      errmsg := fmt.Sprintf("%s dealer waiting for final shuffle: %s", me.Name, err)
      return nil, nil, errors.New(errmsg)
    }
    // dealer starts re-keying
  } else {
    // wait for rekeyed deck from previous player
    rekeydeck, err = recvEDeck(msgspace, prevp, RekeyMsg)
    if err != nil {
      errmsg := fmt.Sprintf("%s getting rekeyed deck: %s", me.Name, err)
      return nil, nil, errors.New(errmsg)
    }
  }

  // re-key cards
  // remove own shuffle key
  unkeyed := cryptAll(rekeydeck, shufkey)
  // encrypt all with unique keys
  rekeyed, finalkeys := cryptEach(unkeyed)
  // send to next player
  err = sendEDeck(nextp, me, RekeyMsg, rekeyed)
  if err != nil {
    return nil, nil, err
  }

  var finaldeck []ECard
  if me.Dealer {
    // dealer should wait for rekeyed card from last player
    finaldeck, err = recvEDeck(msgspace, prevp, RekeyMsg)
    if err != nil {
      errmsg := fmt.Sprintf("%s dealer getting final rekeyed deck: %s", me.Name, err)
      return nil, nil, errors.New(errmsg)
    }
    // and then broadcast the final deck to all players
    err = broadcastEDeck(players, me, FinalDeckMsg, finaldeck)
    if err != nil {
      return nil, nil, err
    }
  } else {
    // other player should wait for dealer to send final deck
    dealer, err := findDealer(players)
    if err != nil {
      return nil, nil, err
    }
    finaldeck, err = recvEDeck(msgspace, dealer, FinalDeckMsg)
    if err != nil {
      errmsg := fmt.Sprintf("%s getting broadcast final deck from dealer: %s", me.Name, err)
      return nil, nil, errors.New(errmsg)
    }
  }

  // enumerate the final deck
  for i, ecard := range finaldeck {
    ecard.DeckPos = i
    finaldeck[i] = ecard
  }

  // yayy we're done
  return finaldeck, finalkeys, nil
}

// encrypt an entire edeck, with the same key
func cryptAll(edeck []ECard, key Pad) []ECard {
  var xoredEdeck []ECard = make([]ECard, len(edeck))
  for i, ecard := range edeck {
    xoredEdeck[i] = crapto.CryptCard(ecard, key)
  }
  return xoredEdeck
}

// encrypt an entire edeck, each
func cryptEach(edeck []ECard) ([]ECard, []Pad) {
  var keys []Pad = make([]Pad, len(edeck))
  var rekeyed []ECard = make([]ECard, len(edeck))
  for i, ecard := range edeck {
    keys[i] = crapto.NewRandomPad()
    rekeyed[i] = crapto.CryptCard(ecard, keys[i])
  }
  return rekeyed, keys
}

// put deck in random order
func shuffleEDeck(edeck []ECard) []ECard {
  // copy edeck into shufdeck
  var shufdeck []ECard = make([]ECard, len(edeck))
  copy(shufdeck, edeck)

  // shuffle shufdeck
  // TODO: better randomness here
  // rand.Seed(time.Now().UnixNano())
  swapfun := func(i, j int) { shufdeck[i], shufdeck[j] = shufdeck[j], shufdeck[i] }
  rand.Shuffle(len(shufdeck), swapfun)
  return shufdeck
}

// send a deck of ECards to a given player
func sendEDeck(dst Player, me Player, msgtype MsgType, edeck []ECard) error {
  edeckjson, err := json.Marshal(edeck)
  if err != nil {
    return err
  }
  comms.SendMsg(dst, me, msgtype, string(edeckjson))
  return nil
}

// broadcast a deck of ECards to a given player
func broadcastEDeck(players []Player, me Player, msgtype MsgType, edeck []ECard) error {
  for _, player := range players {
    err := sendEDeck(player, me, msgtype, edeck)
    if err != nil {
      return err
    }
  }
  return nil
}

// receive shuffled deck from a specific player
func recvEDeck(msgspace gospace.Space, prevp Player, msgtype MsgType) ([]ECard, error) {
  // wait for ShuffleMsg tuple on msgspace
  var fromid, edeckjson string
  var err error
  fromid, edeckjson, err = comms.ExpectMsg(msgspace, msgtype)
  if err != nil {
    return nil, err
  }

  // fail on invalid sender
  if fromid != prevp.Id {
    return nil, errors.New(fmt.Sprintf("Got shuffle from wrong player: %s Expected: %s", fromid, prevp.Id))
  }

  // decode edeck
  var edeck []ECard
  err = json.Unmarshal([]byte(edeckjson), &edeck)
  if err != nil {
    return nil, err
  }

  return edeck, nil
}

// returns the dealer player from list of players
func findDealer(players []Player) (Player, error) {
  for _, player := range players {
    if player.Dealer {
      return player, nil
    }
  }
  return Player{}, errors.New("No dealer found among players")
}
