package gamesetup

/* playorder.go
 * Functions to handle the play order phase of game setup.
 * Dealer selects a random order, and broadcasts this to other players.
 * Other players accept this order.
 */

import (
  // std lib
  "encoding/json"
  "errors"
  "log"
  "math/rand"

  // deps
  "github.com/FelixL321/gospace"

  // app imports
  "../comms"
  . "../types"
)

// select random player order
func playOrderState(msgspace gospace.Space,
  players []Player, me Player) ([]Player, error) {
  if me.Dealer {
    return dealPlayOrder(msgspace, players, me)
  } else {
    return receivePlayerOrder(msgspace, players, me)
  }
}

// as dealer, decide on play order and broadcast it to players
func dealPlayOrder(msgspace gospace.Space,
  players []Player, me Player) ([]Player, error) {
  // select a random order of players
  // players slice is mutated to random order
  // TODO: better randomness here (distributed!!)
  swapfun := func(i, j int) { players[i], players[j] = players[j], players[i] }
  rand.Shuffle(len(players), swapfun)

  // broadcast chosen player order
  playerorderjson, err := json.Marshal(players)
  if err != nil {
    return nil, errors.New("Failed making json from player order.")
  }

  err = comms.BroadcastMsg(players, me, PlayOrderMsg, string(playerorderjson))
  if err != nil {
    return nil, err
  }
  return players, nil
}

// wait for a play order from dealer
func receivePlayerOrder(msgspace gospace.Space,
  players []Player, me Player) ([]Player, error) {
  // wait for an order
  log.Println("SETUP: trying to receive play order.")
  _, playerorderjson, err := comms.ExpectMsg(msgspace, PlayOrderMsg)
  if err != nil {
    return nil, err
  }
  log.Println("SETUP: got playerorder")
  // TODO: more validation here

  // parse the json
  var playerorder []Player
  err = json.Unmarshal([]byte(playerorderjson), &playerorder)
  if err != nil {
    return nil, errors.New("Failed decoding player order from json.")
  }
  if len(playerorder) != len(players) {
    return nil, errors.New("Missing a player when getting player order.")
  }
  return playerorder, nil
}
