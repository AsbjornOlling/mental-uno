package gamesetup

/* decksetup.go
 * Contains functions to create, send and receive
 * the datastructures that represent the deck.
 * (and hashes of same).
 */

import (
  // std lib
  "crypto/sha256"
  "encoding/json"
  "errors"

  // deps
  "github.com/FelixL321/gospace"

  // app imports
  "../comms"
  "../crapto"
  "../gamelogic"
  . "../types"
)

// called by dealer - inits the card datastrutures
func createDeck() (CardMap, []ECard) {
  // make a DCard deck
  var ddeck []DCard = gamelogic.NewDeck()

  // map DCards to CardPads (random data)
  var cmap CardMap = crapto.NewCardMap(ddeck)

  // make an ECard deck from cardpads
  // (deck with no card information, meant to be encrypted)
  var edeck []ECard = make([]ECard, len(ddeck))
  for i, dcard := range ddeck {
    edeck[i] = ECard{Data: cmap.CardPads[dcard]} // XXX: Pos left empty
  } // to be set after shuffle
  return cmap, edeck
}

// only called by dealer - announces sha hashes of card pads to all peers
// sending the hashes ahead of time prevents the dealer changing them after shuffle
// but also prevents the next player from doing a known-plaintext attack
// expects the deck to be ordered as gamelogic.NewDeck() would
func broadcastCardMapHashes(players []Player, me Player,
  edeck []ECard) ([][32]byte, error) {
  // calculate the hashes
  var hashes [][32]byte = make([][32]byte, len(edeck))
  for i, ecard := range edeck {
    hashes[i] = sha256.Sum256(ecard.Data[:])
  }

  // jsonify
  hashesjson, err := json.Marshal(hashes)
  if err != nil {
    return nil, err
  }

  // broadcast the hashes
  err = comms.BroadcastMsg(players, me, PadHashMsg, string(hashesjson))
  if err != nil {
    return nil, err
  }
  return hashes, nil
}

// called by everyone except dealer - wait for pad hashes to arrive from dealer
// these are saved, and later used to verify that the dealer did not manipulate
// the card mappings after shuffle
// expects the hashes to be ordered as gamelogic.NewDeck() would
func recvCardMapHashes(msgspace gospace.Space) ([][32]byte, error) {
  // wait for pad hashes
  _, padhashesjson, err := comms.ExpectMsg(msgspace, PadHashMsg)
  if err != nil {
    return nil, err
  }

  // decode pad hashes
  var padhashes [][32]byte
  err = json.Unmarshal([]byte(padhashesjson), &padhashes)
  if err != nil {
    return nil, err
  }
  return padhashes, nil
}

// called by dealer - share the pad plaintexts with others
func broadcastCardMap(players []Player, me Player, edeckplain []ECard) error {
  // make deck json
  edeckplainjson, err := json.Marshal(edeckplain)
  if err != nil {
    return err
  }
  // broadcast cardmap
  err = comms.BroadcastMsg(players, me, PadShareMsg, string(edeckplainjson))
  if err != nil {
    return err
  }
  return nil
}

// called by everyone except dealer - receive pad plaintexts from dealer,
// verify them, and reconstruct a cardmap
func recvCardMap(msgspace gospace.Space, hashes [][32]byte) (CardMap, error) {
  // receive cardmap from dealer
  _, ecardplaintextsjson, err := comms.ExpectMsg(msgspace, PadShareMsg)
  if err != nil {
    return CardMap{}, err
  }

  // decode card
  var ecardplaintexts []ECard
  err = json.Unmarshal([]byte(ecardplaintextsjson), &ecardplaintexts)
  if err != nil {
    return CardMap{}, err
  }

  // verify pads, build cardmap
  var dcards = gamelogic.NewDeck()
  var dcardmap = make(map[Pad]DCard)
  var cardpadsmap = make(map[DCard]Pad)
  for i, ecard := range ecardplaintexts {
    // check hash, fila on error
    if hashes[i] != sha256.Sum256(ecard.Data[:]) {
      // TODO: broadcast failure/cheat
      return CardMap{}, errors.New("Received ECard plaintext does not match known hash.")
    }
    // populate cardmap
    dcardmap[ecard.Data] = dcards[i]
    cardpadsmap[dcards[i]] = ecard.Data
  }
  return CardMap{DCards: dcardmap,
    CardPads: cardpadsmap}, nil
}
