

test:
	go test -v ./tests/

playtest:
	go test -v tests/gamelogic_test.go tests/setup.go


fixtabs:
	sed -i 's/\t/  /g' ./*/*.go
