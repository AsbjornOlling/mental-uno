package types

type MsgType string

const (
  // general use
  AckMsg MsgType = "ack"

  // used during gamesetup
  PlayOrderMsg MsgType = "playerorder"
  PadHashMsg   MsgType = "padhash"
  ShuffleMsg   MsgType = "shuffle"
  RekeyMsg     MsgType = "rekey"
  FinalDeckMsg MsgType = "finaldeck"
  PadShareMsg  MsgType = "padshare"
  InitCardMsg  MsgType = "initcard"

  // Used doing lobby
  PlayerJoin MsgType = "playerjoin"
  ReturnMsg  MsgType = "returnmsg"
  LobbyJoin  MsgType = "lobbyjoin"
  ChatMsg    MsgType = "chatmsg"
  ReadyMsg   MsgType = "rdymsg"

  // Used during gameplay
  KeyRevealMsg MsgType = "keyreveal"
)
