package types

const HANDSIZE int = 7

// Player is the struct for players
type Player struct {
  Name         string
  Id           string
  Hand         []ECard
  MsgSpaceAddr string
  Dealer       bool
  Uno          bool
  Ready        bool
}

// GameState holds info about current gamestate
type GameState struct {
  // card info
  Deck     []ECard
  DrawPile []ECard
  DeckKeys []Pad
  CardMap  CardMap

  // player info
  Players      []Player
  PlayQueue    []string
  OwnHand      []DCard
  LastPlayedBy string

  // game state
  Lastcard         DCard
  CurrentDrawCount int
  CurrentPlayer    string
  PlusStack        int
  CardDrawn        bool
  CardPlayed       bool
  Done             bool
  WasStacked       bool
  GameStarted      bool

  //misc?
  MenuOptions []bool
  CardOptions []bool
}
