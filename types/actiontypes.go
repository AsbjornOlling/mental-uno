package types

type ActionType string

// the four game actions identifiers
const (
  PlayAction              ActionType = "play"
  DrawAction              ActionType = "draw"
  PassAction              ActionType = "pass"
  UnoAction               ActionType = "uno"
  ChallengePlusFourAction ActionType = "challengep4"
  ChallengeUnoAction      ActionType = "challengeuno"
)

// parameters that come with Play
type PlayParams struct {
  Card    DCard
  DeckPos int
  Color   CardColor
  HandPos int
}

// parameters that come with Draw
type DrawParams struct {
  DeckPos int
}

type Action struct {
  AType   ActionType
  PParams PlayParams
  DParams DrawParams
}

// no parameters come with Pass or Uno
