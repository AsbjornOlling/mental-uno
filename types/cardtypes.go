package types

//CardColor Enum types for Color
type CardColor int

//CardType enum
type CardType int

//Colors of cards
const (
  Red    CardColor = 0
  Blue   CardColor = 1
  Yellow CardColor = 2
  Green  CardColor = 3
  Wild   CardColor = 4
)

//Card types
const (
  Zero     CardType = 0
  One      CardType = 1
  Two      CardType = 2
  Three    CardType = 3
  Four     CardType = 4
  Five     CardType = 5
  Six      CardType = 6
  Seven    CardType = 7
  Eight    CardType = 8
  Nine     CardType = 9
  Skip     CardType = 10
  Reverse  CardType = 11
  PlusTwo  CardType = 12
  PlusFour CardType = 13
  Change   CardType = 14
)

// DCard is the struct for decrypted cards
type DCard struct {
  Id    int  // Id just used to provide uniqueness
  Color CardColor
  Type  CardType
}

// ECard is an encrypted card
type ECard struct {
  DeckPos  int // position in shuffled deck
  Data Pad     // encrypted cardpad
}
