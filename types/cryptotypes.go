package types

// keysize for OTP
const PADSIZE = 256

// the type used for OTP plaintext and keys 
type Pad [PADSIZE]byte

// CardMap maps the large card
type CardMap struct {
  DCards   map[Pad]DCard
  CardPads map[DCard]Pad
}
