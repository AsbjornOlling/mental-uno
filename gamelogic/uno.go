package gamelogic

import (
  "../comms"
  "../player"
  . "../types"
)

// react to incoiming UnoAction from another player
func IncomingUno(gs GameState, player Player) (GameState, error) {
  return Uno(gs, player)
}

// tell other players about this players' uno action
func OutgoingUno(gs GameState, me Player, seqno int) (GameState, error) {
  err := comms.BroadcastAction(gs.Players, me, seqno, UnoAction, "")
  if err != nil {
    return GameState{}, err
  }
  curplayer, err := player.PlayerById(gs.Players, gs.CurrentPlayer)
  if err != nil {
    return GameState{}, err
  }
  return Uno(gs, curplayer)
}
