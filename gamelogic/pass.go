package gamelogic

import (
  // app imports
  "../comms"
  . "../types"
)

// handle incoming PassAction from another player
func IncomingPass(gs GameState, player Player) (GameState, error) {
  return Pass(gs, player, false)
}

// tell other players about pass action
func OutgoingPass(gs GameState, me Player, seqno int) (GameState, error) {
  // broadcast update to other players
  err := comms.BroadcastAction(gs.Players, me, seqno, PassAction, "")
  if err != nil {
    return GameState{}, err
  }

  // update gamestate
  gs, err = Pass(gs, me, false)
  if err != nil {
    return GameState{}, err
  }
  return gs, nil
}
