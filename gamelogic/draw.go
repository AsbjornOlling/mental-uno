package gamelogic

import (
  // std lib
  "encoding/json"
  "errors"
  "fmt"

  // deps
  "github.com/FelixL321/gospace"

  // app imports
  "../comms"
  "../crapto"
  . "../types"
)

// handle DrawAction from another player
func IncomingDraw(msgspace gospace.Space, gs GameState, me Player,
  player Player, params DrawParams) (GameState, error) {
  // pick and encode key
  drawkey := gs.DeckKeys[params.DeckPos]
  drawkeyjson, err := json.Marshal(drawkey)
  if err != nil {
    return GameState{}, err
  }

  // send key to drawing player
  err = comms.SendMsg(player, me, KeyRevealMsg, string(drawkeyjson))
  if err != nil {
    return GameState{}, err
  }

  // update local gamestate
  gs, err = Draw(gs, player, gs.Deck[params.DeckPos])
  if err != nil {
    return GameState{}, err
  }

  // wait for drawer
  _, err = comms.ExpectMsgFrom(msgspace, AckMsg, player.Id)
  return gs, nil
}

// tell other players about draw action,
// get keys for decrypting the drawn card
// decrypt the card, and add it to gs.OwnHand
func OutgoingDraw(msgspace gospace.Space, gs GameState, me Player,
  seqno int, params DrawParams) (GameState, error) {
  // serialize DrawParams
  paramsjson, err := json.Marshal(params)
  if err != nil {
    return GameState{}, err
  }

  // send to all players
  err = comms.BroadcastAction(gs.Players, me, seqno,
    DrawAction, string(paramsjson))
  if err != nil {
    return GameState{}, err
  }

  // expect keys from all
  var keysjson []string
  keysjson, err = comms.ExpectMsgFromAll(msgspace, gs.Players, me, KeyRevealMsg)
  if err != nil {
    errmsg := fmt.Sprintf("Error while getting keys for draw: %s", err)
    return GameState{}, errors.New(errmsg)
  }

  // deserialize keys
  var keys []Pad = make([]Pad, len(gs.Players))
  for i, keyjson := range keysjson {
    err = json.Unmarshal([]byte(keyjson), &keys[i])
    if err != nil {
      errmsg := fmt.Sprintf("Error while decoding keys: %s", err)
      return GameState{}, errors.New(errmsg)
    }
  }

  // add own key
  keys[len(keys)-1] = gs.DeckKeys[params.DeckPos]

  // decrypt card
  var dcard DCard
  dcard, err = crapto.DecryptCard(gs.CardMap, gs.Deck[params.DeckPos], keys)
  if err != nil {
    return GameState{}, err
  }

  // add decrypted card to own hand
  gs.OwnHand = append(gs.OwnHand, dcard)

  // update local gamestate
  gs, err = Draw(gs, me, gs.Deck[params.DeckPos])
  if err != nil {
    return GameState{}, err
  }

  // let players know that card is received and all is well
  err = comms.BroadcastMsg(gs.Players, me, AckMsg, "")
  if err != nil {
    return GameState{}, err
  }
  return gs, nil
}
