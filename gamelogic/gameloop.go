package gamelogic

import (
  // std lib
  "encoding/json"
  "errors"
  "fmt"
  "log"

  // deps
  "github.com/FelixL321/gospace"

  // app imports
  "../comms"
  "../frontend"
  "../art"
  "../player"
  . "../types"
)

/* gameloop.go
 * Contains the master logic of executing a game of uno.
 * Provides both the main gameloop (listen -> execute -> repeat)
 * and the implementation of each play-by-play protocol.
 */

//* game loop,
func GameLoop(msgspace gospace.Space, gs GameState, me Player) {
  var err error
  var playerid string
  var actiontype ActionType
  var actiondata string
  var seqno int = 0
  gs.GameStarted = true

  // print hand on game start
  art.PrintHand(gs.OwnHand)
  for {

    if gs.CurrentPlayer == me.Id {
      // print hand
      art.PrintHand(gs.OwnHand)

      // update menu options
      gs = UpdateMenuOptions(gs, me)

      // get user choice
      newaction := frontend.ActionMenu(gs)

      // talk to other players about action
      // and update own gamestate
      gs, err = handleOutgoingAction(msgspace, gs, me, seqno, newaction)
      if err != nil {
        log.Fatal(err)
      }
    } else {
      // read action from other player
      playerid, actiontype, actiondata, err = comms.ReadAction(msgspace, seqno)
      if err != nil {
        log.Fatal(err)
      }

      // handle action from other player
      // and update gamestate
      gs, err = handleIncomingAction(msgspace, gs, me,
        playerid, actiontype, actiondata)
      if err != nil {
        log.Fatal(err)
      }
    }
    seqno++
    if gs.Done {
      return
    }
  }
} // */

// call function appropriate to outgoing action
// return gamestate on success
func handleOutgoingAction(msgspace gospace.Space, gs GameState,
  me Player, seqno int, action Action) (GameState, error) {
  switch action.AType {
  case DrawAction:
    return OutgoingDraw(msgspace, gs, me, seqno, action.DParams)

  case PlayAction:
    return OutgoingPlay(gs, me, seqno, action.PParams)

  case PassAction:
    return OutgoingPass(gs, me, seqno)

  case UnoAction:
    return OutgoingUno(gs, me, seqno)
  }
  return GameState{}, errors.New(fmt.Sprintf("Invalid action: %s\n", action.AType))
}

// call function appropriate to incoming action
// return gamestate on success
func handleIncomingAction(msgspace gospace.Space, gs GameState,
  me Player, playerid string,
  action ActionType, actiondata string) (GameState, error) {
  // find player
  player, err := player.PlayerById(gs.Players, playerid)
  if err != nil {
    log.Fatal(err)
  }
  log.Printf("GAME: %s sends action: %s", player.Name, action)

  // validate player
  if playerid != gs.CurrentPlayer {
    errmsg := fmt.Sprintf("Player %s played out of turn.\n", player.Name)
    return GameState{}, errors.New(errmsg)
  }

  // pass incoming action on to appropriate fiuction
  switch action {
  case DrawAction:
    var dparams DrawParams
    err = json.Unmarshal([]byte(actiondata), &dparams)
    if err != nil {
      return GameState{}, err
    }
    return IncomingDraw(msgspace, gs, me, player, dparams)

  case PlayAction:
    var pparams PlayParams
    err = json.Unmarshal([]byte(actiondata), &pparams)
    if err != nil {
      return GameState{}, err
    }
    return IncomingPlay(msgspace, gs, me, player, pparams)

  case PassAction:
    return IncomingPass(gs, player)

  case UnoAction:
    return IncomingUno(gs, player)
  }
  return GameState{}, errors.New(fmt.Sprintf("Invalid action: %s\n", action))
}
