package gamelogic

import (
  // std lib
  "encoding/json"
  "errors"
  "fmt"
  "reflect"

  // deps
  "github.com/FelixL321/gospace"

  // app imports
  "../comms"
  "../crapto"
  "../player"
  . "../types"
)

// - send keys to everyone except player
// - get keys from everyone
// - encrypt dcard with all keys
func IncomingPlay(msgspace gospace.Space, gs GameState, me Player,
  player Player, params PlayParams) (GameState, error) {
  // assemble list of players to send key
  // (everyone except self and card player)
  var keyreceivers []Player = make([]Player, len(gs.Players)-2)
  var i int = 0
  for _, p := range gs.Players {
    if p.Id == me.Id || p.Id == player.Id {
      // don't add self and card player to key receivers
      continue
    }
    keyreceivers[i] = p
    i++
  }

  // deserialize key
  var mykey Pad = gs.DeckKeys[params.DeckPos]
  keyjson, err := json.Marshal(mykey)
  if err != nil {
    return GameState{}, err
  }

  // send key to others
  err = comms.BroadcastMsg(keyreceivers, me,
    KeyRevealMsg, string(keyjson))
  if err != nil {
    return GameState{}, err
  }

  // expect keys from everyone
  var keysjson []string
  keysjson, err = comms.ExpectMsgFromAll(msgspace, gs.Players, me, KeyRevealMsg)
  if err != nil {
    return GameState{}, err
  }

  // build keys slice with everybody's keys
  var keys []Pad = make([]Pad, len(gs.Players))
  for i, keyjson := range keysjson {
    var key Pad
    err := json.Unmarshal([]byte(keyjson), &key)
    if err != nil {
      return GameState{}, err
    }
    keys[i] = key
  }
  // append own key
  keys[len(keys)-1] = mykey

  // encrypt N times
  var ecard ECard = crapto.EncryptCard(gs.CardMap, params.Card, keys)

  // check card against player's hand
  var found bool = false
  for _, handecard := range player.Hand {
    if reflect.DeepEqual(handecard.Data, ecard.Data) {
      found = true
      break
    }
  }
  if !found {
    errmsg := fmt.Sprintf("Card played by %s not found in hand.", player.Name)
    return GameState{}, errors.New(errmsg)
  }

  // update gamestate
  return CardPlayed(gs, player, params, me.Id)
}

// me plays a card
// - broadcast action
// - share key for card with others
// - update gamestate
func OutgoingPlay(gs GameState, me Player,
  seqno int, params PlayParams) (GameState, error) {
  // marshal as json
  paramsjson, err := json.Marshal(params)
  if err != nil {
    return GameState{}, err
  }

  // send json to others
  err = comms.BroadcastAction(gs.Players, me, seqno,
    PlayAction, string(paramsjson))
  if err != nil {
    return GameState{}, err
  }

  // deserialize played card key
  keyjson, err := json.Marshal(gs.DeckKeys[params.DeckPos])
  if err != nil {
    return GameState{}, err
  }

  // send key for card to all
  err = comms.BroadcastMsg(gs.Players, me,
    KeyRevealMsg, string(keyjson))
  if err != nil {
    return GameState{}, err
  }

  // update gamestate
  curplayer, _ := player.PlayerById(gs.Players, gs.CurrentPlayer)
  return CardPlayed(gs, curplayer, params, me.Id)
}
