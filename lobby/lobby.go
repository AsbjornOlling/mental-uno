/*
 * This is the Mental UNO game lobby,
 * players will wait here till all players are ready.
 *
 */

package lobby

import (
  // std lib
  "encoding/json"
  "log"
  "strings"

  // deps
  . "github.com/FelixL321/gospace"
  "github.com/google/uuid"

  // app imrpots
  . "../comms"
  "../frontend"
  "../types"
)

func Lobby(host string) (players []types.Player, spc Space) {
  // Init lobbyspace
  uri := strings.Join([]string{"tcp://", host}, "")
  spc = NewSpace(uri)

  // Init player
  players = make([]types.Player, 1)
  initPlayer(uri, &players[0])

  // Gametype logic
  // Choose game type
  gametype := frontend.PrintGametypeMenu(players[0].Name)

  // Logic for starting a game
  switch gametype {
    case 1:
      NewGameLobby(&players, host, spc)
    // Logic for joining a game
    case 2:
      joinGame(&players, host, spc)
  }

  // Finally wait for more players or until all players are ready
  waitrdy(spc, &players)
  return players, spc
}


// Init Player
func initPlayer(host string, p *types.Player) {
  nick := frontend.PrintWelcome()
  *p = types.Player{Name: nick,
    Id:           uuid.New().String(),
    MsgSpaceAddr: host}
}

// Start a game
func NewGameLobby(players *[]types.Player, host string, spc Space) {
  // Make me dealer
  (*players)[0].Dealer = true
  frontend.StartGameLobby(host)
  // Waiting for first player
  _, msgdata, _ := ExpectMsg(spc, types.PlayerJoin)
  var readplayer types.Player
  err := json.Unmarshal([]byte(msgdata), &readplayer)
  if err != nil {
    log.Print("Change json string to []byte failed, due to ", err)
    return
  }
  *players = append(*players, readplayer)
  frontend.PrintJoinedLobby((*players)[1].Name)
  // Send my p struct back
  err = SendMsg((*players)[1], (*players)[0], types.ReturnMsg, ptojson(*players))
  if err != nil {
    log.Fatal("Could not send player json, due to: ", err)
  }
}

// Join a game
func joinGame(players *[]types.Player, host string, spc Space) {
  // Define remote host
  remhost := frontend.RemHostMenu()
  uri := strings.Join([]string{"tcp://", remhost}, "")
  // Format my player struct
  *players = append(*players, types.Player{MsgSpaceAddr: uri})
  var playerjson []byte
  playerjson, err := json.Marshal((*players)[0])
  if err != nil {
    log.Fatal("Could not decode player to json: ", err)
  }
  me := string(playerjson)
  // Connect to remote host and send my player struct
  frontend.PrintConnTo(remhost)
  err = SendMsg((*players)[1], (*players)[0], types.PlayerJoin, me)
  if err != nil {
    log.Fatal("Could not send, due to: ", err)
  }
  // Wait for returned players struct from host
  _, msgdata, err := ExpectMsg(spc, types.ReturnMsg)
  if err != nil {
    log.Fatal("Could not read, due to: ", err)
  }
  // Add recived player structs to my players slice
  var readplayers []types.Player
  err = json.Unmarshal([]byte(msgdata), &readplayers)
  if err != nil {
    log.Print("Change json string to []byte failed, due to ", err)
    return
  }
  // The first player in recived data should be the player after me
  (*players)[1] = readplayers[0]
  frontend.PrintJoinedLobbyOf((*players)[1].Name, host)
  // If there is more players in the hosts struct, I should also add those
  if len(readplayers) >= 2 {
    for i := 1; i < len(readplayers); i++ {
      if readplayers[i].Id != (*players)[0].Id {
        *players = append(*players, readplayers[i])
        frontend.PrintJoinedLobby(readplayers[i].Name)
      }
    }
  }
}

func waitrdy(spc Space, players *[]types.Player) {
  ch := make(chan bool)

  go waitPlayer(spc, players, ch)

  for !(*players)[0].Ready {
    // This menu is just while waiting for all ready or new players
    // Only case 1 is important, and case 2 is nice for debug, case 3 is just for fun.
    menuopt := frontend.WaitReadyMenu()
    switch menuopt {
    case 1:
      (*players)[0].Ready = true
      // Broadcast Ready
      BroadcastMsg(*players, (*players)[0], types.ReadyMsg, "Ready")
      var allready bool = true
      for i := 0; i < len(*players); i++ {
        if !(*players)[i].Ready {
          allready = false
          break
        }
      }
      if allready {
        SendMsg((*players)[0], (*players)[0], types.ReadyMsg, "Ready")
        break
      }

    case 2:
      for i := 0; i < len(*players); i++ {
        if (*players)[i].Ready == true {
          frontend.PrintReadyPlayer((*players)[i])
        } else {
          frontend.PrintNotReadyPlayer((*players)[i])
        }
      }
    case 3:
      BroadcastMsg(*players, (*players)[0], types.ChatMsg, frontend.PrintSendChat())
    }
  }
  // wait for goroutine to complete
  <-ch
}

// This function waits for new players, ready messages or lobbyjoins
func waitPlayer(spc Space, players *[]types.Player, ch chan bool) {
  defer func(){ ch <- true }()
  for {
    // Get a new message
    userid, msgtype, msgdata, _ := ReadMsg(spc)

    // Check the msgtype of the msg
    // PlayerJoin is a new player joining my lobby
    if msgtype == types.PlayerJoin {
      var readplayer types.Player
      err := json.Unmarshal([]byte(msgdata), &readplayer)
      if err != nil {
        log.Print("Change json string to []byte failed, due to ", err)
        return
      }

      *players = append(*players, readplayer)
      frontend.PrintJoinedLobby((*players)[len(*players)-1].Name)

      // Send my p slice back (ReturnMsg)
      err = SendMsg((*players)[len(*players)-1], (*players)[0], types.ReturnMsg, ptojson(*players))
      if err != nil {
        log.Fatal("Could not send player json, due to: ", err)
      }

      // Broadcast new player to rest of the lobby (LobbyJoin)
      err = BroadcastMsg(*players, (*players)[0], types.LobbyJoin, ptojson(*players))
      if err != nil {
        log.Fatal("Could not send player json, due to: ", err)
      }
    } else if msgtype == types.LobbyJoin {
      // When reciving a LobbyJoin I only add it to my players slice
      var readplayers []types.Player
      err := json.Unmarshal([]byte(msgdata), &readplayers)
      if err != nil {
        log.Print("Change json string to []byte failed, due to ", err)
        return
      }

      for i := 0; i < len(readplayers); i++ {
        newplayer := true
        for j := 0; j < len(*players); j++ {
          if readplayers[i].Id == (*players)[j].Id {
            newplayer = false
            break
          }
        }
        if newplayer {
          *players = append(*players, readplayers[i])
          frontend.PrintJoinedLobby((*players)[len(*players)-1].Name)
        }
      }
    } else if msgtype == types.ReadyMsg {
      // TODO: Update player
      for i := 0; i < len(*players); i++ {
        if userid == (*players)[i].Id {
          (*players)[i].Ready = true
          frontend.PrintReadyPlayer((*players)[i])
        }
      }

      // return if all players are ready
      var allready bool = true
      for i := 0; i < len(*players); i++ {
        if !(*players)[i].Ready {
          allready = false
          break
        }
      }
      if allready {
        return
      }

    } else if msgtype == types.ChatMsg {
      for i := 0; i < len(*players); i++ {
        if (*players)[i].Id == userid {
          frontend.PrintRecvChat((*players)[i].Name, msgdata)
        }
      }
    }
  }
}

// Helper function
// Player to json
func ptojson(players []types.Player) (playerSlice string) {
  var playersjson []byte
  playersjson, err := json.Marshal(players)
  if err != nil {
    log.Fatal("Could not decode player to json: ", err)
  }
  playerSlice = string(playersjson)
  return playerSlice
}


/* Lobby notes:
 * Player   --PlayerJoin-->   Host   --LobbyJoin-->   Lobby   --RdyMsg-->   Game init
 * Player  <--ReturnMsg ---   Host
 * When a player enters my space (PlayerJoin), I check if he is in my players slice,
 * if not I add him and Broadcast his struct (Lobb¶yJoin). I also send my players slice back to the new player (ReturnMsg)
 * to update his players slice.
 *
 * When a LobbyJoin enters my space, I check if he is in my players slice,
 * if no I add him. No need to Broadcast.
 *
 * When I send a msg it is just broadcasted to my player slice.
 *
 * When I send a Rdy msg, I broadcast the message to my slice, and then I check if the rest of my slice is Rdy, if so I go to  * init state.
 *
 * When I recive a Rdy message, I check if the rest of my slice is Rdy (Inc. myself), if so I go to init state.
 */
