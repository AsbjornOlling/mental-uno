package player

import (
  // std lib
  "encoding/json"
  "errors"
  "log"

  // app imports
  . "../types"
)

// deep copy of a list of players
// because FUCK
// yeah fuck you
func CopyPlayers(players []Player) ([]Player, error) {
  playersjson, err := json.Marshal(players)
  if err != nil {
    return nil, err
  }
  var newplayers []Player
  err = json.Unmarshal([]byte(playersjson), &newplayers)
  if err != nil {
    return nil, err
  }
  return newplayers, nil
}

// find player by id in a player slice
func PlayerById(players []Player, id string) (Player, error) {
  for _, p := range players {
    if id == p.Id {
      return p, nil
    }
  }
  return Player{}, errors.New("No player found with that ID.")
}

// return list of IDs from list of players
func PlayerIds(players []Player) []string {
  var ids []string = make([]string, len(players))
  for i, player := range players {
    ids[i] = player.Id
    log.Println("DEBUG: player id in Playerids:", player.Id)
  }
  return ids
}

// returns the player whose turn is after the given palyer,
// and the player whose turn is before the given player
func NextAndPrev(players []Player, curp Player) (Player, Player, error) {
  // get turn number of given player
  var meidx int = -1
  for i, p := range players {
    if curp.Id == p.Id {
      meidx = i
      break
    }
  }
  if meidx == -1 {
    return Player{}, Player{},
      errors.New("Player not found in given player slice.")
  }
  // get the next player
  nextidx := (meidx + 1) % len(players)
  nextp := players[nextidx]
  // get the previous player
  previdx := ((meidx - 1) + len(players)) % len(players)
  prevp := players[previdx]

  // return that stuff
  return nextp, prevp, nil
}
